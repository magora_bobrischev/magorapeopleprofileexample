package com.example.grey.profileexample;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public interface ScrollDispatcher {

    boolean canBeScrolled();
}
