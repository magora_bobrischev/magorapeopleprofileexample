package com.example.grey.profileexample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ContentHolder extends RecyclerView.ViewHolder {
    private ContentHolder(View itemView) {
        super(itemView);
    }

    public static ContentHolder createHolder(ViewGroup parent) {
        return new ContentHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_content, parent, false));
    }
}
