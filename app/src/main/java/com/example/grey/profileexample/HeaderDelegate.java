package com.example.grey.profileexample;

import android.app.Activity;
import android.content.res.Resources;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class HeaderDelegate
        implements AppBarLayout.OnOffsetChangedListener {
    private static final float CONTENT_SCALE = 0.7f;
    private static final float ALPHA_PERCENT_MULTIPLIER = 1.75f;
    private final AppBarLayout appBarLayout;
    private final View avatar;
    private final View vgKeysInfo;
    private final View tvUserName;
    private final View tvPosition;
    private final View tvDepartment;
    private final View tvOffice;

    private final int collapsedHeight;
    private final int statusBarHeight;
    private final int avatarCollapsedMarginTop;

    public HeaderDelegate(@NonNull Activity activity) {
        appBarLayout = (AppBarLayout) activity.findViewById(R.id.app_bar_layout);

        avatar = activity.findViewById(R.id.iv_photo);
        vgKeysInfo = activity.findViewById(R.id.vg_keys_info);
        tvUserName = activity.findViewById(R.id.tv_username);
        tvPosition = activity.findViewById(R.id.tv_position);
        tvDepartment = activity.findViewById(R.id.tv_department);
        tvOffice = activity.findViewById(R.id.tv_host_office);


        statusBarHeight = getStatusBarHeight(activity.getResources());
        collapsedHeight = appBarLayout.getChildAt(0).getMinimumHeight() + statusBarHeight;
        avatarCollapsedMarginTop = ((ViewGroup.MarginLayoutParams) avatar.getLayoutParams()).topMargin / 2;
    }

    public void resume() {
        appBarLayout.addOnOffsetChangedListener(this);
    }

    public void pause() {
        appBarLayout.removeOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (avatar == null) {
            return;
        }
        View tmpAvatar = avatar;
        View tmpKeysInfo = vgKeysInfo;
        View tmpUserName = tvUserName;
        View tmpPosition = tvPosition;
        View tmpDepartment = tvDepartment;
        View tmpOffice = tvOffice;

        int collapsedStateOffset = appBarLayout.getMeasuredHeight() - collapsedHeight;

        float percent = Math.abs(verticalOffset / (float) collapsedStateOffset);
        float alphaPercent = constraintValue(percent * ALPHA_PERCENT_MULTIPLIER);

        float avatarScaledSize = tmpAvatar.getMinimumWidth();
        float avatarTargetScale = avatarScaledSize / tmpAvatar.getMeasuredHeight();
        float collapsedMarginTop = statusBarHeight + avatarCollapsedMarginTop * avatarTargetScale;

        float avatarFinalPositionY = collapsedStateOffset + collapsedMarginTop;
        float offsetY = percent * (avatarFinalPositionY - avatar.getTop());

        tmpAvatar.setY(tmpAvatar.getTop() + offsetY);

        float scale = 1 - (1 - avatarTargetScale) * percent;
        tmpAvatar.setScaleX(scale);
        tmpAvatar.setScaleY(scale);

        float alpha = 1 - 1 * alphaPercent;
        float viewEndY;

        if (tmpKeysInfo.getVisibility() == View.VISIBLE) {
            viewEndY = avatarFinalPositionY + avatarScaledSize;
            tmpKeysInfo.setY(tmpKeysInfo.getTop() + percent * (viewEndY - tmpKeysInfo.getTop()));

            float keysScaledSize = tmpKeysInfo.getMinimumHeight();
            float keysTargetScale = keysScaledSize / tmpKeysInfo.getMeasuredHeight();
            scale = 1 - (1 - keysTargetScale) * percent;
            tmpKeysInfo.setScaleX(scale);
            tmpKeysInfo.setScaleY(scale);
            tmpKeysInfo.setAlpha(alpha);
        }

        viewEndY = avatarFinalPositionY + avatarScaledSize;
        tmpUserName.setY(tmpUserName.getTop() + percent * (viewEndY - tmpUserName.getTop()));

        float nameScaledSize = tmpUserName.getMinimumHeight();
        float nameTargetScale = nameScaledSize / tmpUserName.getMeasuredHeight();
        scale = 1 - (1 - nameTargetScale) * percent;
        tmpUserName.setScaleX(scale);
        tmpUserName.setScaleY(scale);

        scale = 1 - (1 - CONTENT_SCALE) * percent;
        viewEndY += nameScaledSize;
        tmpPosition.setY(tmpPosition.getTop() + percent * (viewEndY - tmpPosition.getTop()));
        tmpPosition.setScaleY(scale);
        tmpPosition.setScaleX(scale);

        alphaPercent = constraintValue(percent * (ALPHA_PERCENT_MULTIPLIER + 1f));
        alpha = constraintValue(1 - 1 * alphaPercent);
        tmpPosition.setAlpha(alpha);

        viewEndY += tmpPosition.getMeasuredHeight() * CONTENT_SCALE;
        tmpDepartment.setY(tmpDepartment.getTop() + percent * (viewEndY - tmpDepartment.getTop()));
        tmpDepartment.setScaleY(scale);
        tmpDepartment.setScaleX(scale);

        alphaPercent = constraintValue(percent * (ALPHA_PERCENT_MULTIPLIER + 2.2f));
        alpha = constraintValue(1 - 1 * alphaPercent);
        tmpDepartment.setAlpha(alpha);

        viewEndY += tmpDepartment.getMeasuredHeight() * CONTENT_SCALE;
        tmpOffice.setY(tmpOffice.getTop() + percent * (viewEndY - tmpOffice.getTop()));
        tmpOffice.setScaleY(scale);
        tmpOffice.setScaleX(scale);

        alphaPercent = constraintValue(percent * (ALPHA_PERCENT_MULTIPLIER + 3.0f));
        alpha = constraintValue(1 - 1 * alphaPercent);
        tmpOffice.setAlpha(alpha);
    }

    public boolean isHeaderCollapsed() {
        return appBarLayout != null && appBarLayout.getBottom() == collapsedHeight;
    }

    private static int getStatusBarHeight(Resources resources) {
        int result = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @FloatRange(from = 0, to = 1)
    private static float constraintValue(float value) {
        if (value < 0f) {
            return 0f;
        } else if (value > 1f) {
            return 1f;
        }
        return value;
    }
}
