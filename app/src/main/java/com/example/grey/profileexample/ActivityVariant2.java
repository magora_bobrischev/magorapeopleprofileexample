package com.example.grey.profileexample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.ViewGroup;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */

public class ActivityVariant2 extends AppCompatActivity implements ScrollDispatcher {
    private HeaderDelegate headerDelegate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_variant_2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        headerDelegate = new HeaderDelegate(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.nested_scroll);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return ContentHolder.createHolder(parent);
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            }

            @Override
            public int getItemCount() {
                return 1;
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.isInLayout() && !canBeScrolled()) {
                    recyclerView.scrollToPosition(0);
                }
            }
        });

        recyclerView.setOnClickListener(v -> {
            int id = v.getId();
            Log.i("M_D", id + "");
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        headerDelegate.resume();
    }

    @Override
    protected void onPause() {
        headerDelegate.pause();
        super.onPause();
    }

    @Override
    public boolean canBeScrolled() {
        return headerDelegate.isHeaderCollapsed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }
}
