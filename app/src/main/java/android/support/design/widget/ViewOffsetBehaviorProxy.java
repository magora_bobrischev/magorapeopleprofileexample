package android.support.design.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2017.
 */
public class ViewOffsetBehaviorProxy<V extends View> extends ViewOffsetBehavior<V> {

    public ViewOffsetBehaviorProxy() {
    }

    public ViewOffsetBehaviorProxy(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
